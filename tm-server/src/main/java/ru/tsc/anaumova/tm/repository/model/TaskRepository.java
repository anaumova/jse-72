package ru.tsc.anaumova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @Nullable
    Task findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull List<Task> findAllByProjectIdAndUserId(@NotNull String userId, @NotNull String projectId);

    void deleteAllByUserId(@NotNull String userId);

}