package ru.tsc.anaumova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.dto.model.ProjectDto;

import java.util.List;

@Repository
public interface ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> {

    @NotNull
    List<ProjectDto> findAllByUserId(@NotNull String userId);

    @Nullable
    ProjectDto findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteAllByUserId(@NotNull String userId);

}