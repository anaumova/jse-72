package ru.tsc.anaumova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.dto.model.ProjectDto;
import ru.tsc.anaumova.tm.dto.request.ProjectListRequest;
import ru.tsc.anaumova.tm.dto.response.ProjectListResponse;
import ru.tsc.anaumova.tm.enumerated.Sort;
import ru.tsc.anaumova.tm.event.ConsoleEvent;
import ru.tsc.anaumova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Show project list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectListListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[PROJECT LIST]");
        System.out.println("[ENTER SORT:]");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken(), sort);
        @NotNull final ProjectListResponse response = getProjectEndpoint().listProject(request);
        @Nullable final List<ProjectDto> projects = response.getProjects();
        int index = 1;
        for (ProjectDto project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

}