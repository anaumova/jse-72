package ru.tsc.anaumova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.dto.request.UserUpdateRequest;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.event.ConsoleEvent;
import ru.tsc.anaumova.tm.util.TerminalUtil;

@Component
public final class UserUpdateListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-update";

    @NotNull
    public static final String DESCRIPTION = "Update user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userUpdateListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ENTER FIRST NAME:]");
        @NotNull String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        @NotNull String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME:]");
        @NotNull String middleName = TerminalUtil.nextLine();
        getUserEndpoint().updateUser(new UserUpdateRequest(getToken(), firstName, lastName, middleName));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}